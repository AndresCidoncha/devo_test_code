variable "region" {
  default = "eu-west-1"
  type    = string
}

variable "ubuntu_version" {
  default     = "20.04"
  type        = string
  description = "Ubuntu version to get AMI for"
}

variable "instance_type" {
  default     = "t3.nano"
  type        = string
  description = "EC2 instance type"
}

variable "key_name" {
  type        = string
  description = "Key Pair name to use in instance creation"
}

variable "service" {
  type        = string
  default     = "amqp_checker"
  description = "Service name to be used as hostname and tags"
}

variable "instance_count" {
  default     = 1
  type        = number
  description = "Number of instances to be created"
}

variable "root_volume" {
  default     = {}
  type        = map(string)
  description = "EC2 EBS root device configs"
}

variable "allowed_ips" {
  type        = list(string)
  default     = []
  description = "CIDR blocks to allow access to the instance"
}

variable "allowed_ssh_ips" {
  type        = list(string)
  default     = []
  description = "CIDR blocks to allow access to the instance"
}

variable "listen_port" {
  default     = 13000
  type        = number
  description = "API port number"
}

variable "ssh_user" {
  default     = "devops"
  type        = string
  description = "SSH user"
}

variable "ssh_port" {
  default     = 13579
  type        = number
  description = "SSH port number"
}

variable "environment" {
  type        = string
  default     = "dev"
  description = "Environment"
}
