locals {
  deploy_prefix  = "${var.service}-${var.environment}"
  templates_path = "${path.module}/cloud-init-templates"
  templates_vars = {
    default_user = var.ssh_user,
    ssh_port     = var.ssh_port,
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-*-${var.ubuntu_version}-amd64-server-*"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = { Name = local.deploy_prefix }
}

resource "aws_route" "main" {
  route_table_id         = aws_vpc.main.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_security_group" "amqp_checker" {
  name        = local.deploy_prefix
  description = "Allow AMQP CHECKER inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Endpoint access"
    from_port   = var.listen_port
    to_port     = var.listen_port
    protocol    = "tcp"
    cidr_blocks = var.allowed_ips
  }

  ingress {
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = var.allowed_ssh_ips
    description = "Custom SSH port"
  }

  egress {
    description = "Internet access"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = { Name = local.deploy_prefix }
}

data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

  dynamic "part" {
    for_each = fileset(local.templates_path, "*.tpl")
    content {
      content_type = "text/cloud-config"
      content      = templatefile("${local.templates_path}/${part.value}", local.templates_vars)
    }
  }
}

resource "aws_instance" "instance" {
  count = var.instance_count

  ami              = data.aws_ami.ubuntu.id
  instance_type    = var.instance_type
  key_name         = var.key_name
  user_data_base64 = data.template_cloudinit_config.config.rendered
  metadata_options {
    http_tokens = "required"
  }

  subnet_id                   = aws_subnet.main.id
  vpc_security_group_ids      = [aws_security_group.amqp_checker.id]
  associate_public_ip_address = true

  ebs_optimized = true
  root_block_device {
    encrypted             = true
    volume_type           = lookup(var.root_volume, "type", "gp2")
    volume_size           = tonumber(lookup(var.root_volume, "size", "20"))
    delete_on_termination = tobool(lookup(var.root_volume, "delete_on_termination", "true"))
  }

  tags        = { Name = format("%s-%02d", local.deploy_prefix, count.index + 1) }
  volume_tags = { Name = format("%s-%02d", local.deploy_prefix, count.index + 1) }

  lifecycle {
    ignore_changes = [ # Ignore disk changes and new AMI versions
      ami,
      root_block_device
    ]
  }
}
