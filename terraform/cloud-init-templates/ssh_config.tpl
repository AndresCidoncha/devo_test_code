#cloud-config
runcmd:
  - [ sed, -i, -e, "/^#*[\\s]*Port\\s.*/s//Port ${ssh_port}/", /etc/ssh/sshd_config ]
  - [ sudo, service, sshd, restart ]
