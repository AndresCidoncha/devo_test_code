output "ssh_port" {
  value = var.ssh_port
}

output "ssh_user" {
  value = var.ssh_user
}

output "listen_port" {
  value = var.listen_port
}

output "public_ips" {
  value = aws_instance.instance.*.public_ip
}

output "private_ips" {
  value = aws_instance.instance.*.private_ip
}
