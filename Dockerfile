FROM golang:1.16-alpine AS builder

RUN apk update && apk add --no-cache git=2.34.4-r0

COPY . /code
WORKDIR /code/amqp_checker
RUN go mod tidy && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build

FROM scratch

ENV USER=appuser
ENV UID=10001

ENV LISTEN_ADDRESS="0.0.0.0"
ENV LISTEN_PORT="3000"

EXPOSE 3000

WORKDIR /
COPY --from=builder /code/amqp_checker/amqp_checker /amqp_checker

ENTRYPOINT ["/amqp_checker"]
