package main

import (
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
)

const VERSION = "1.0.0"

func GetenvDefault(envName string, defaultValue string) string {
	value := os.Getenv(envName)
	if len(value) == 0 {
		value = defaultValue
	}
	return value
}

func LogLevel(levelName string) log.Level {
	logLevel := map[string]log.Level{
		"DEBUG": log.DebugLevel,
		"INFO":  log.InfoLevel,
		"WARN":  log.WarnLevel,
		"ERROR": log.ErrorLevel,
	}

	if level, exists := logLevel[levelName]; exists {
		return level
	}

	return log.InfoLevel
}

func FileExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func LoadYaml(path string) []byte {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Errorf("Can't load config file from %s: %v ", path, err)
		return nil
	}

	return content
}

func Coalesce(a string, b string) string {
	if len(a) > 0 {
		return a
	} else {
		return b
	}
}
