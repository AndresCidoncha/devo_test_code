package main_test

import (
	"os"
	"testing"

	main "amqp_checker"

	log "github.com/sirupsen/logrus"

	"github.com/stretchr/testify/assert"
)

func TestGetEnvConfigDefault(t *testing.T) {
	expected_config := &main.Config{
		LogLevel:      "INFO",
		AmqpServerURL: "amqp://localhost:5672",
		ListenAddr:    "localhost",
		ListenPort:    "3000",
	}
	result := main.GetEnvConfig()
	assert.Equal(t, expected_config, result)
}

func TestGetEnvConfig(t *testing.T) {
	expected_config := &main.Config{
		LogLevel:      "DEBUG",
		AmqpServerURL: "amqp://localhost:5672",
		ListenAddr:    "0.0.0.0",
		ListenPort:    "4000",
	}
	os.Setenv("LOG_LEVEL", "DEBUG")
	os.Setenv("LISTEN_ADDRESS", "0.0.0.0")
	os.Setenv("LISTEN_PORT", "4000")
	result := main.GetEnvConfig()
	assert.Equal(t, expected_config, result)
}

func TestMergeConfigs(t *testing.T) {
	a := &main.Config{
		LogLevel:      "INFO",
		AmqpServerURL: "amqp://localhost:5672",
		ListenAddr:    "0.0.0.0",
		ListenPort:    "4000",
	}
	b := &main.Config{
		LogLevel:      "DEBUG",
		AmqpServerURL: "amqp://localhost:5672",
		ListenAddr:    "1.1.1.1",
		ListenPort:    "",
	}
	expected_config := &main.Config{
		LogLevel:      "DEBUG",
		AmqpServerURL: "amqp://localhost:5672",
		ListenAddr:    "1.1.1.1",
		ListenPort:    "4000",
	}
	result := main.MergeConfigs(a, b)
	assert.Equal(t, expected_config, result)
}

func TestGetFileConfigNotFound(t *testing.T) {
	expected_config := &main.Config{
		LogLevel:      "INFO",
		AmqpServerURL: "amqp://localhost:9999",
		ListenAddr:    "1.1.1.1",
		ListenPort:    "3000",
	}
	result := main.GetFileConfig("config.yml", expected_config)
	assert.Equal(t, expected_config, result)
}

func TestGetFileConfigError(t *testing.T) {
	expected_config := &main.Config{
		LogLevel:      "DEBUG",
		AmqpServerURL: "amqp://localhost:5672",
		ListenAddr:    "0.0.0.0",
		ListenPort:    "4000",
	}
	result := main.GetFileConfig("test/error_config.yml", expected_config)
	assert.Equal(t, expected_config, result)
}

func TestGetFileConfig(t *testing.T) {
	input_config := &main.Config{
		LogLevel:      "INFO",
		AmqpServerURL: "amqp://localhost:15672",
		ListenAddr:    "localhost",
		ListenPort:    "3000",
	}
	expected_config := &main.Config{
		LogLevel:      "DEBUG",
		AmqpServerURL: "amqp://localhost:5672",
		ListenAddr:    "0.0.0.0",
		ListenPort:    "4000",
	}
	result := main.GetFileConfig("test/config.yml", input_config)
	assert.Equal(t, expected_config, result)
}

func TestConfig(t *testing.T) {
	expected_config := &main.Config{
		LogLevel:      "DEBUG",
		AmqpServerURL: "amqp://localhost:5672",
		ListenAddr:    "0.0.0.0",
		ListenPort:    "4000",
	}
	result := main.GetConfig()
	assert.Equal(t, expected_config, result)
}

func TestCheckAMQPServer(t *testing.T) {
	serverUrl := "amqp://localhost:5672"
	if len(os.Getenv("CI")) > 0 {
		serverUrl = "amqp://rabbitmq:5672"
		log.Infof("RUNNING IN CI ENVIRONMENT. SERVER URL WILL BE %s", serverUrl)
	}
	result := main.CheckAMQPServer(serverUrl)
	assert.True(t, result)
	result = main.CheckAMQPServer("test")
	assert.False(t, result)
}

func TestGetBodyResponse(t *testing.T) {
	expected_true := main.Response{Running: true}
	result := main.GetBodyResponse(true)
	assert.Equal(t, expected_true, result)
	expected_false := main.Response{Running: false}
	result = main.GetBodyResponse(false)
	assert.Equal(t, expected_false, result)
}

func TestGetFiberApp(t *testing.T) {
	params := map[string]string{"amqpServerURL": "foo"}
	result := main.GetFiberApp(params)
	config := result.Config()
	assert.Contains(t, config.AppName, "AMQP Checker v")
	assert.Equal(t, "AMQP Checker", config.ServerHeader)
}

func TestGetListenAddress(t *testing.T) {
	config := &main.Config{
		ListenAddr: "9.9.9.9",
		ListenPort: "99999",
	}
	result := main.GetListenAddress(config)
	assert.Equal(t, "9.9.9.9:99999", result)
}
