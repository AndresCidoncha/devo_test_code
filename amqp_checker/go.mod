module amqp_checker

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.35.0
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.6.1
	golang.org/x/sys v0.0.0-20220712014510-0a85c31ab51e // indirect
	gopkg.in/yaml.v3 v3.0.1
)
