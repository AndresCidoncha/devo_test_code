package main_test

import (
	"os"
	"testing"

	log "github.com/sirupsen/logrus"

	main "amqp_checker"

	"github.com/stretchr/testify/assert"
)

func TestCoalesce(t *testing.T) {
	result := main.Coalesce("A", "B")
	assert.Equal(t, "A", result)
	result = main.Coalesce("", "B")
	assert.Equal(t, "B", result)
}

func TestGetenvDefault(t *testing.T) {
	result := main.GetenvDefault("TEST", "A")
	assert.Equal(t, "A", result)
	os.Setenv("TEST", "B")
	result = main.GetenvDefault("TEST", "A")
	assert.Equal(t, "B", result)
}

func TestLogLevel(t *testing.T) {
	result := main.LogLevel("DEBUG")
	assert.Equal(t, log.DebugLevel, result)
	result = main.LogLevel("TEST")
	assert.Equal(t, log.InfoLevel, result)
}

func TestFileExists(t *testing.T) {
	result := main.FileExists("config.test")
	assert.False(t, result)
	result = main.FileExists("test/config.yml")
	assert.True(t, result)
}

func TestLoadYaml(t *testing.T) {
	result := string(main.LoadYaml("test/config.yml"))
	assert.Contains(t, result, "listenAddr: 0.0.0.0")
	assert.Nil(t, main.LoadYaml("test.yml"))
}
