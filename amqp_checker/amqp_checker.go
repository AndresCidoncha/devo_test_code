package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"

	"github.com/streadway/amqp"
)

type Response struct {
	Running bool
}

type Config struct {
	LogLevel      string `yaml:"logLevel"`
	AmqpServerURL string `yaml:"amqpServerURL"`
	ListenAddr    string `yaml:"listenAddr"`
	ListenPort    string `yaml:"listenPort"`
}

func GetEnvConfig() *Config {
	config := &Config{
		LogLevel:      GetenvDefault("LOG_LEVEL", "INFO"),
		AmqpServerURL: GetenvDefault("AMQP_SERVER_URL", "amqp://localhost:5672"),
		ListenAddr:    GetenvDefault("LISTEN_ADDRESS", "localhost"),
		ListenPort:    GetenvDefault("LISTEN_PORT", "3000"),
	}
	return config
}

func MergeConfigs(a *Config, b *Config) *Config {
	config := &Config{
		LogLevel:      Coalesce(b.LogLevel, a.LogLevel),
		AmqpServerURL: Coalesce(b.AmqpServerURL, a.AmqpServerURL),
		ListenAddr:    Coalesce(b.ListenAddr, a.ListenAddr),
		ListenPort:    Coalesce(b.ListenPort, a.ListenPort),
	}
	return config
}

func GetFileConfig(path string, config *Config) *Config {
	if FileExists(path) {
		fileConfig := &Config{}
		content := LoadYaml(path)
		if err := yaml.Unmarshal(content, fileConfig); err != nil {
			log.Errorf("Unmarshal: %v", err)
			return config
		}
		config = MergeConfigs(config, fileConfig)
	}
	return config
}

func CheckAMQPServer(serverUrl string) bool {
	if connectAMQP, err := amqp.Dial(serverUrl); err == nil {
		defer connectAMQP.Close()
		return true
	}
	return false
}

func GetBodyResponse(running bool) Response {
	response := Response{
		Running: running,
	}

	return response
}

func GetFiberApp(params map[string]string) *fiber.App {
	app := fiber.New(fiber.Config{
		ServerHeader: "AMQP Checker",
		AppName:      fmt.Sprintf("AMQP Checker v%s", VERSION),
	})

	app.Use(
		logger.New(),
	)

	log.Debugf("params: %+v", params)
	app.Get("/", func(c *fiber.Ctx) error {
		response := GetBodyResponse(CheckAMQPServer(params["amqpServerURL"]))
		log.Debugf("Response: %+v", response)
		return c.JSON(response)
	})

	return app
}

func GetConfig() *Config {
	config := GetEnvConfig()
	configPath := GetenvDefault("CONFIG_PATH", "config.yml")
	config = GetFileConfig(configPath, config)
	log.Infof("config: %+v", config)

	return config
}

func GetListenAddress(config *Config) string {
	listenAddress := fmt.Sprintf("%s:%s", config.ListenAddr, config.ListenPort)
	log.Debugf("listenAddress: %s", listenAddress)

	return listenAddress
}

func main() {
	config := GetConfig()
	log.SetLevel(LogLevel(config.LogLevel))

	params := map[string]string{"amqpServerURL": config.AmqpServerURL}
	app := GetFiberApp(params)

	log.Infof("Running amqp_checker against %s", config.AmqpServerURL)
	app.Listen(GetListenAddress(config))
}
