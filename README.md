# DEVO Technical Challenge

- [DEVO Technical Challenge](#devo-technical-challenge)
  - [Intro](#intro)
  - [Requirements](#requirements)
  - [Running Tests](#running-tests)
    - [Go](#go)
  - [Build container](#build-container)
  - [Deployment](#deployment)
    - [Running Terraform](#running-terraform)
    - [Running Ansible](#running-ansible)
  - [Pipeline](#pipeline)

## Intro

This repo contains the code for the DEVO technical challenge for DevOps Engineer.

The main goal is to develop and deploy a simple app that monitors an MQ service connection.

Requirements:

- Simple API with just an index endpoint that returns the MQ service connection status.
- API must be configurable by config file.
- Development must include tests.
- All development must be based in a CI/CD pipeline.
- Provide Terraform code to deploy app container in a server.
- API's configuration file must be created using Ansible.

Bonus:

- Deployment in K8s.
- Design and deploy app and the MQ service in HA.

## Requirements

- Terraform >= 1.1
- Golang >=1.16
- Ansible >=2.9.6

## Running Tests

### Go

Running API tests in local can be achieved using the following commands:

```shell
cd amqp_checker # Enter in the code folder
go mod tidy # Install and be sure that dependencies are up to date
go test -race
go test -coverprofile=coverage.out && go tool cover -html=coverage.out -o coverage.html # Get coverage report in HTML
```

Current development contains only unit testing and security checks.

## Build container

Building the container in local can be done by running:

```shell
docker build -t amqp_checker:latest .
```

## Deployment

### Running Terraform

Requirements:

- configured AWS credentials for running session

You also must have a terraform variables file with CIDR lists to allow access:

```conf
key_name    = "test"
allowed_ips = ["0.0.0.0/0"]      # To access API
allowed_ssh_ips = ["0.0.0.0/0"]  # To access SSH
```

Inside the terraform folder, you must run:

```shell
terraform plan  # or terraform apply to create infra
```

Running this code will create:

- VPC
  - VPC
  - Subnet
  - Internet Gateway
  - Route Table with default route
  - Security Group
- EC2
  - Instance

The outputs from applying this code will be:

- ssh_user: Configured SSH user
- ssh_port: Configured SSH port
- listen_port: Opened port in SH for API incoming traffic.
- public_ips: List of public ips of created instances.
- private_ips: List of privated ips of created instances.

### Running Ansible

Install `community.docker` module:

```shell
ansible-galaxy collection install community.docker
```

Add created instances in the `/etc/ansible/hosts`:

```conf
[amqp_checker]
<IP> ansible_user=<ssh_user> ansible_port=<ssh_port> ansible_ssh_private_key_file=<path_to_keypair_pem>
```

You must add a line for each ip returned in the `public_ips` terraform output. `ssh_user` and `ssh_port` must match values for terraform output too.

Check access to the hosts:

```shell
ansible amqp_checker -m ping
```

Run playbook against the hosts:

```shell
ansible-playbook playbook.yml --vault-password-file ~/.vault_password.txt
```

## Pipeline

Current pipeline runs the following steps:

- Test
  - Go code test (with coverage report)
  - Terraform validate
  - Dockerfile lint
  - Ansible Playbook lint
- Build
  - Docker image build
